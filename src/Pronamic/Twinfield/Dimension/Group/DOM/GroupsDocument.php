<?php
namespace Pronamic\Twinfield\Dimension\Group\DOM;

use \Pronamic\Twinfield\Dimension\Group\Group;

/**
 * The Document Holder for making new XML groups.  Is a child class
 * of DOMDocument and makes the required DOM for the interaction in
 * creating a new invoice.
 *
 * @package Pronamic\Twinfield
 * @subpackage Dimension\Group\DOM
 * @author Leon Rowland <leon@rowland.nl>
 * @copyright (c) 2013, Pronamic
 * @version 0.0.1
 */
class GroupsDocument extends \DOMDocument
{
    // @todo implement
    public function addGroup(){}
}
