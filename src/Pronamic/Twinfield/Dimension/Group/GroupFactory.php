<?php
namespace Pronamic\Twinfield\Dimension\Group;

use \Pronamic\Twinfield\Factory\ParentFactory;
use \Pronamic\Twinfield\Invoice\Mapper\InvoiceMapper;
use \Pronamic\Twinfield\Request as Request;

/**
 * GroupFactory
 *
 * A facade factory to make interaction with the twinfield service easier
 * when trying to retrieve or set information about Groups.
 *
 * Each method has detailed explanation over what is required, and what
 * happens.
 *
 * If you require more complex interactions or a heavier amount of control
 * over the requests to/from then look inside the methods or see the
 * advanced guide details the required usages.
 *
 * @package Pronamic\Twinfield
 * @subpackage Dimension/Group
 */
class GroupFactory extends ParentFactory
{
    public function send(Group $group)
    {
        // Attempts to process the login
        if ($this->getLogin()->process()) {

            // Gets the secure service
            $service = $this->getService();

            // Gets a new instance of InvoicesDocument and sets the invoice
            $groupsDocument = new DOM\GroupsDocument();
            $groupsDocument->addGroup($group);

            // Sends the DOM document request and sets the response
            $response = $service->send($groupsDocument);
            $this->setResponse($response);

            // Return a bool on status of response
            if ($response->isSuccessful()) {
                return true;
            } else {
                return false;
            }
        }
    }
}
